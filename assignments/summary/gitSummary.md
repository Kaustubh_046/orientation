# Git Basics in a Nutshell #

## What is git? ##

* Git is version-control software that makes collaboration with teammates super simple.
* Git is the way we manage code and there is no software development without git.

## Basic Git Lingo ##

#### Repository/Repo: ####
It’s the big box you and your team throw your code into. Collection of files and folders related to your code.

#### Commit: ####
When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.

#### Push: ####
Pushing is essentially syncing your commits to GitLab.

#### Branch: ####
Think of your git repo as a tree, then the branches of the tree are called branches.  
 The trunk of the tree, the main software, is called the Master Branch.  

#### Merge: ####
When your branch is ready to become part of the primary codebase, it will get merged into the master branch.

#### Clone: ####
Cloning a repo is pretty much exactly what it sounds like. It takes the entire online repository and makes an exact copy of it on your local machine.

#### Fork: ####
Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name. 

## How to install Git ##
For Linux, open the terminal and type   
    `sudo apt-get install git`
 
On Windows, it’s just as simple. You [download the installer](https://git-scm.com/download/win) and run it.

## Git Internals ##
 Basic Commands to move your files into different trees in your repo
![Basic Commands to move your files into different trees in your repo](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)

## Git Workflow ##
* Cloning the repo   
`$ git clone <link-to-repository> `
* Creating a new branch   
`$ git checkout master`
`$ git checkout -b <your-branch-name>`
* You selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.  
`$ git add .         # To add untracked files ( . adds all files)`
* You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repository.  
`$ git commit -sv   # Description about the commit`
* You do a push, which takes the files as they are in the Local Git Repository and stores that snapshot permanently to your Remote Git Repository.  
`$ git push origin <branch-name>      # push changes into repository`
